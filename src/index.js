const path = require("path");
const express = require("express");
const { engine } = require("express-handlebars");
var morgan = require("morgan");
const app = express();
const port = 3000;
const route = require("./routes/");
// In order for server to serve static files, we have to use,
app.use(express.static(path.join(__dirname, "public")));

// Middleware
// xu li du lieu tu form data html
app.use(
    express.urlencoded({
        extended: true,
    })
);
// xu li du lieu tu javascript
app.use(express.json());

// HTTP logger
app.use(morgan("combined"));

// template engine
app.engine(
    "hbs",
    engine({
        extname: ".hbs",
    })
);
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "resource/views"));
// app.set("views", "./views");

// Route init
route(app);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});
